{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c3a8c172-39b0-490f-a51e-584ba0b0adb2",
   "metadata": {},
   "source": [
    "# Getting information from the user"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "d4401bdc-4c9c-4a6d-a835-0f8331abd395",
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [],
   "source": [
    "%load_ext interactive_system_magic"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fb44d9e-b7d1-4119-b40d-cb23793da61a",
   "metadata": {},
   "source": [
    "When running scripts, you often want them to be able to do different things in different situations. Take, for example, a script which prints a greeting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "7b2b78c2-9d26-4d10-abde-366ed0503244",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting greeting.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile greeting.py\n",
    "\n",
    "print(\"Hello Matt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "a4e8fec9-4ea9-4afe-9826-6b51051a9e72",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Hello Matt"
      ]
     },
     "execution_count": 3,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da436bca-515f-425d-bd13-e04a06f5f09e",
   "metadata": {},
   "source": [
    "We can make it a little easier to modify by extracting the name out into a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "1970a2ba-0783-4daf-bd66-54d98e60584f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting greeting.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile greeting.py\n",
    "\n",
    "name = \"Matt\"\n",
    "print(f\"Hello {name}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a8c99913-c886-4504-a4a3-561d30048396",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Hello Matt"
      ]
     },
     "execution_count": 5,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad8f0e3b-1e62-4927-9cd4-387faba784d5",
   "metadata": {},
   "source": [
    "but still, no matter how many times we run this script, it will always give the same result. If you want it to be able to print `Hello Sam` for example then you'll need to manually edit the script.\n",
    "\n",
    "Editing scripts like this can be error-prone and if you have multiple things you need to change, it can be hard to keep track of."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e04cfa52-fa64-41d9-99f4-6c9dd46141bf",
   "metadata": {},
   "source": [
    "There are many different ways you can handle this problem, and each is appropriate in different situations. We've already seen *hard-coding* and this is only appropriate for variables which will never change or are used as defaults.\n",
    "\n",
    "This chapter will introduce reading input interactively and command-line arguments as two ways you can read in information from the user."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27322d87-0afd-45e6-b1a7-7d86c26bec83",
   "metadata": {},
   "source": [
    "## Interactive input\n",
    "\n",
    "Talking with the user of your script interactively is useful when you want to be able to ask a series of questions and respond to their previous answers. It's also sometimes easier for non-technical users to understand as you can guide them through the process.\n",
    "\n",
    "The core function we use is [`input`](https://docs.python.org/3/library/functions.html#input) which prints some *prompt* text and then pauses the program to let the user type in an answer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "cca1bf7a-e7b3-430b-a327-ce6ebeecdaa9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting greeting.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile greeting.py\n",
    "\n",
    "name = input(\"What is your name? \")\n",
    "print(f\"Hello {name}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "edfbb1b2-84e2-4a8f-8c0b-b00d68bc7875",
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "source": [
    "Running this will prompt you to give an answer which is then used in the rest of the script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "920cb8fc-a12d-4528-bc8f-e051683eb0e9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "What is your name? Matt\n",
       "Hello Matt"
      ]
     },
     "execution_count": 7,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%run_python_script -i greeting.py\n",
    "Matt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27805a99-180c-4c93-9114-076ae1536967",
   "metadata": {},
   "source": [
    "Creating your scripts to be interactive like this is useful and is sometimes the right choice. However, you will fins that it's generally better if you can run your script automatically and unattended. You don't want to set your analysis script running, leave it to get on with it, only to return a few hours later to find it's been sitting there the whole time waiting for an input."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4999a3ab-e0d5-4def-9f64-c7d1b096852a",
   "metadata": {},
   "source": [
    "## Command-line arguments\n",
    "\n",
    "One of the most common ways to get information into your program is via command-line arguments. These allow you to have one script which you can run in many different ways, depending on context. This is particularly useful if you want to be able to run your Python code on a batch computer, a HPC system or as part of a workflow or pipeline.\n",
    "\n",
    "The starting place for command-line argument is the variable [https://docs.python.org/3/library/sys.html#sys.argv](`argv` from the `sys` module). This variable is automatically created by Python when you run it and it contains a list of the arguments that were passed to your script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "42fafea6-4059-40dd-8416-25ba32029940",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting args.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile args.py\n",
    "\n",
    "import sys\n",
    "\n",
    "print(sys.argv)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f28a419-4a77-4eac-8e30-5e4ea55fd1e2",
   "metadata": {},
   "source": [
    "If we run the script with no additional arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "22e07e3b-3a11-46eb-b9e6-920262188b79",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['args.py']"
      ]
     },
     "execution_count": 9,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python args.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script args.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13f988f1-9eeb-437c-ae44-934a445c178a",
   "metadata": {},
   "source": [
    "we see that by default it is a list with a single element, that of the name of the script that was run.\n",
    "\n",
    "If we run it again with more arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "2bab97f1-a1cf-4c22-9433-c965666d0fd3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['args.py', 'these', 'are', 'arguments']"
      ]
     },
     "execution_count": 10,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python args.py these are arguments",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script args.py these are arguments"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4fed1af-71f5-4daa-a662-59bb0499f4d3",
   "metadata": {},
   "source": [
    "then we see those turn up in the list as well. Notice that it is using spaces between the arguments that were passed in to separate the arguments. This is why you should avoid having spaces in your file names as the two parts of the name will be split:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "6bda4d87-c3e9-4af5-ad93-c2364d2fc09c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['args.py', 'C:/Program', 'Files']"
      ]
     },
     "execution_count": 11,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python args.py C:/Program Files",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script args.py C:/Program Files"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6b43a34-6492-4e90-b7bd-7c2f1988bbde",
   "metadata": {},
   "source": [
    "As `sys.argv` is a simple Python list, you can access the parts of it individually, so let's update our greeting script to take the name as a command-line argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "e2f16fd8-2e4c-4d04-a560-4199b91bc77b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting greeting.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile greeting.py\n",
    "\n",
    "import sys\n",
    "\n",
    "name = sys.argv[1]\n",
    "print(f\"Hello {name}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "915760f6-cce8-4d88-a27d-4464b61e3204",
   "metadata": {},
   "source": [
    "Note that the argument we're grabbing is at index `1` since index `0` is always the *name* of the script."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "7e952ee4-e41f-4f8c-a83b-5411e1e12db4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Hello Matt"
      ]
     },
     "execution_count": 13,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py Matt",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py Matt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df98223c-2140-465c-aea9-5584fc28d5f7",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "1. Get this greeting script working for yourself.\n",
    "2. What happens if you run it with no command-line arguments?\n",
    "3. Can you get it to print a name with a space in it? Like `Hello Matt Williams`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09d964c1-3e53-4290-b228-2cfc0ef8b809",
   "metadata": {},
   "source": [
    "## Introducing the `argparse` module\n",
    "\n",
    "With enough care and liberal use of `if` statements and `for` loops, you would be able to write code which can understand complex sets of arguments such as:\n",
    "```\n",
    "pip install --upgrade -r requirements.txt -r requirements-dev.txt\n",
    "```\n",
    "or\n",
    "```\n",
    "find . -type f -exec grep -il 'foo' {} \\\n",
    "```\n",
    "but as you might imagine, that would get pretty tricky.\n",
    "\n",
    "Luckily, Python provides us with a module which is able to look at and understand the passed arguments (a process called *parsing*) and interpret them for us. [`argparse`](https://docs.python.org/3/library/argparse.html) is a standard library module which allows you to define what argument and flags are accepted by your script and to automatically parse `sys.argv` for you.\n",
    "\n",
    "After importing the module, you create an [`ArgumentParser`](https://docs.python.org/3/library/argparse.html#argumentparser-objects):\n",
    "```python\n",
    "parser = argparse.ArgumentParser()\n",
    "```\n",
    "\n",
    "This gives you an object to which you can describe what you want to accept.\n",
    "\n",
    "It handles lots of things, but for now we just want to tell it that it u take a single argument which we will refer to as `name` using the [`add_argument`](https://docs.python.org/3/library/argparse.html#the-add-argument-method) method:\n",
    "```python\n",
    "parser.add_argument(\"name\")\n",
    "```\n",
    "finally, you ask it to parse the arguments in `sys.argv` with\n",
    "```python\n",
    "args = parser.parse_args()\n",
    "```\n",
    "\n",
    "which you can then access with\n",
    "```python\n",
    "args.name\n",
    "```\n",
    "\n",
    "Using this, we can update our `greeting.py` file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "3cc94ebc-fecc-437f-a935-bcacf210fcd1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting greeting.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile greeting.py\n",
    "\n",
    "import argparse\n",
    "\n",
    "parser = argparse.ArgumentParser()\n",
    "parser.add_argument(\"name\")\n",
    "\n",
    "args = parser.parse_args()\n",
    "\n",
    "print(f\"Hello {args.name}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d7b36d4-41e0-4570-962c-a23850967bf3",
   "metadata": {},
   "source": [
    "This means that we can call our script in the same way as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "cff052ed-9ffb-44f5-acd8-6d600a4fae78",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Hello Matt"
      ]
     },
     "execution_count": 15,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py Matt",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py Matt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29531926-c53a-436e-8ccc-0bab06972624",
   "metadata": {},
   "source": [
    "But now when we call it with no argument, instead of an `IndexError` we get a nice helpful message:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "e7e44213-0869-421f-88e1-3153135c2124",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "usage: greeting.py [-h] name\n",
       "greeting.py: error: the following arguments are required: name"
      ]
     },
     "execution_count": 16,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py",
       "returncode": 2
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2aebae95-de9d-4893-82af-0481057994c4",
   "metadata": {},
   "source": [
    "It's automatically created a `--help` (or equivalently `-h`) flag to print some more detailed help:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "c6c40c4e-ca87-4021-97f8-eb50e9087a59",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "usage: greeting.py [-h] name\n",
       "\n",
       "positional arguments:\n",
       "  name\n",
       "\n",
       "options:\n",
       "  -h, --help  show this help message and exit"
      ]
     },
     "execution_count": 17,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py --help",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdc055d5-e6f0-4264-a201-8eee560ba521",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit `greeting.py` to accept two arguments, one for the name and another for the weather, such that you can run\n",
    "```\n",
    "python greeting.py Matt sunny\n",
    "```\n",
    "and it will print out:\n",
    "```\n",
    "Hello Matt, today it is sunny.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57f76281-3cb0-42de-8621-aa070c942cfa",
   "metadata": {},
   "source": [
    "### Adding helpful extra info\n",
    "\n",
    "By default, the help text printed out by the `--help` flag contains just the minimum information. It's really helpful to users of your script if you give extra information here to explain what the various arguments and flags mean:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "a490ec05-6ceb-474c-8b42-1d0414fbcb61",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting greeting.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile greeting.py\n",
    "\n",
    "import argparse\n",
    "\n",
    "parser = argparse.ArgumentParser(description=\"A greeting program\")\n",
    "parser.add_argument(\"name\", help=\"The name to say 'hello' to\")\n",
    "\n",
    "args = parser.parse_args()\n",
    "\n",
    "print(f\"Hello {args.name}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "df860f0f-ca66-4363-bc83-8ab3a714486d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "usage: greeting.py [-h] name\n",
       "\n",
       "A greeting program\n",
       "\n",
       "positional arguments:\n",
       "  name        The name to say 'hello' to\n",
       "\n",
       "options:\n",
       "  -h, --help  show this help message and exit"
      ]
     },
     "execution_count": 19,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/python_scripts/venv/bin/python greeting.py --help",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script greeting.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87da3d34-1493-435b-b06f-96004397665e",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit your `greeting.py` script to add a description for the `weather` positional argument."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79fb68c0-cffc-4f5c-8b51-d8ca6f08fd24",
   "metadata": {},
   "source": [
    "## Types"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8bb1bc6-5d9b-4616-b3fe-8263d8d4e375",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile multiply.py\n",
    "\n",
    "import argparse\n",
    "\n",
    "parser = argparse.ArgumentParser(description=\"A greeting program\")\n",
    "parser.add_argument(\"a\", help=\"The first value to multiply\")...\n",
    "\n",
    "args = parser.parse_args()\n",
    "\n",
    "print(f\"Hello {args.name}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb6ab14c-e3c7-4017-ac76-defcc6a466f8",
   "metadata": {},
   "source": [
    "## Other resources\n",
    "\n",
    "Another good module for reading command line arguments is the [Command Line Interface Creation Kit (`click`)](https://click.palletsprojects.com).\n",
    "\n",
    "You might also want to look into defining inputs to your program through the use of configuration files, using something like [`configparser`](https://docs.python.org/3/library/configparser.html)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
